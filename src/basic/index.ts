import { Rule, SchematicContext, Tree } from "@angular-devkit/schematics";

// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function basic(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const paths = path("hello-world");
    for (const pat of paths) {
      tree.create(pat, "🎱");
    }
    return tree;
  };
}

export const path = (path: string): string[] => {
  return [
    `/module/${path}/${path}.html`,
    `/module/${path}/${path}.ts`,
    `/module/${path}/${path}.spec.ts`,
    `/module/${path}/${path}.css`
  ];
};
